# Plone testing base docker image

Use this for your docker based Plone tests.

Following is an example `.gitlab-ci.yml` file:

```yaml
image: itspirit4docker/plone-testing-base:latest

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache"

cache:
  paths:
    - .cache
    - venv/
    - downloads/
    - eggs/

stages:
  - test
  - report

before_script:
  - python -V
  - virtualenv --clear -p python3.7 venv
  - source venv/bin/activate
  - pip install -r requirements.txt -c constraints.txt
  - buildout -n -c buildout.cfg download-cache=downloads code-analysis:return-status-codes=True

code-analysis:
  stage: test
  script:
    - bin/code-analysis

unittests:
  stage: test
  script:
    - bin/test

robot:
  stage: test
  script:
    - export DISPLAY=:99.0
    - xvfb-run bin/test --all

coverage:
  stage: report
  script:
    - bin/createcoverage
    - bin/coverage html
    - bin/coverage report
  when: on_success

  coverage: '/TOTAL\s+\d+\s+\d+\s+(\d+\%)/'
  artifacts:
    paths:
      - htmlcov
```