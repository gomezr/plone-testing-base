FROM python:3.7-slim-stretch

ENV GECKODRIVER=v0.26.0

LABEL os="debian" \
    os.version="9" \
    name="Plone Testing" \
    description="Plone base image for testing Plone applications." \
    maintainer="it-spirit"

RUN apt-get update \
    && apt-get install -y apt-utils locales \
    && echo "en_US UTF-8" > /etc/locale.gen \
    && locale-gen en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
ENV LC_CTYPE en_US.UTF-8

RUN echo "deb http://ftp.de.debian.org/debian/ stretch main contrib non-free" >> /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y --no-install-recommends wget build-essential firefoxdriver xvfb xauth \
    && wget https://github.com/mozilla/geckodriver/releases/download/${GECKODRIVER}/geckodriver-${GECKODRIVER}-linux64.tar.gz \
    && tar -xzf geckodriver-${GECKODRIVER}-linux64.tar.gz -C /usr/local/bin \
    && rm geckodriver-${GECKODRIVER}-linux64.tar.gz \
    && chmod +x /usr/local/bin/geckodriver \
    && pip install virtualenv \
    && apt-get -y autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["bash"]
